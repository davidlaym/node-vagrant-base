# NODE BASE
This is a vagrant file and provision sh scripts to raise a vagrant VM on VirtualBox and
set it up to develop nodejs apps.

## What's in

* MongoDB (apt)
* nodejs (apt)
* bower (npm)
* gulp (npm)
* forever (npm)

## Required vagrant plugins

This script takes advantage of the following plugins:

~~
$ vagrant plugin install vagrant-hostsupdater
~~

## Ussage

All platforms shoud take into consideration that this script generates
a host entry on the host for the guest, so you may want to change the
hostname to accomodate your project.

Just find in `Vagrantfile` and in `sh/provision.sh` instances of `node.dev`
and change that to whatever you like (it's recomendable to keep the `.dev` TLD or use a `.test` TLD)

### Windows
Please run vagrant up in an elevated (Administrative) command prompt.
